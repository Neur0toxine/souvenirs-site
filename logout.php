<?php
require_once __DIR__ . '/app/config.php';
if($GLOBALS['users']->isLoggedIn()) {
    $GLOBALS['users']->logout();
    header('Location: /');
    die();
}
?>
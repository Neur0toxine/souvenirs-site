<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/UsersController.php';
require_once __DIR__ . '/PostsController.php';
require_once __DIR__ . '/CategoriesController.php';
require_once __DIR__ . '/RequestParser.php';

// Загрузка конфигурации
$env = new Dotenv\Dotenv(__DIR__, '/../config.cfg');
$env->load();

// Определение режима отладки
if(getenv('DEBUG', false))
{
    ini_set('display_errors', On);
    ini_set('error_reporting', E_ALL);
}
else
{
    ini_set('display_errors', Off);
    ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT);
}

// Подключение к БД и настройка ORM.
$pdo = new \PDO("mysql:dbname=" . getenv('DB_NAME', 'database') . ";host=" . getenv('DB_HOST', '127.0.0.1'), getenv('DB_USER', 'user'), getenv('DB_PASSWORD', 'password'));
$GLOBALS['dbh'] = new \LessQL\Database( $pdo );
$GLOBALS['dbh']->setReference( 'posts', 'categories', 'categories_id' );

$GLOBALS['bbcode'] = new JBBCode\Parser();


// Определение тегов bbcode
$builder = new JBBCode\CodeDefinitionBuilder('image', '<img class="d-block w-0 h-auto mx-auto my-1" src="{param}">');
$builder->setParseContent(false);
$GLOBALS['bbcode']->addCodeDefinition($builder->build());

$builder = new JBBCode\CodeDefinitionBuilder('header', '<h2 class="mt-2 mb-1 d-inline-block">{param}</h2>');
$builder->setParseContent(false);
$GLOBALS['bbcode']->addCodeDefinition($builder->build());

$builder = new JBBCode\CodeDefinitionBuilder('s', '<s>{param}</s>');
$builder->setParseContent(false);
$GLOBALS['bbcode']->addCodeDefinition($builder->build());

$builder = new JBBCode\CodeDefinitionBuilder('p', '<p class="m-0 d-inline-block">{param}</p>');
$builder->setParseContent(true);
$GLOBALS['bbcode']->addCodeDefinition($builder->build());

$GLOBALS['bbcode']->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());


// Загрузка контроллеров
$GLOBALS['posts'] = new PostsController($GLOBALS['dbh']);
$GLOBALS['users'] = new UsersController($GLOBALS['dbh']);
$GLOBALS['categories'] = new CategoriesController($GLOBALS['dbh']);


// Загрузка и настройка шаблонизатора
$tpl = new League\Plates\Engine(__DIR__ . '/../views');
$tpl->loadExtension(new League\Plates\Extension\Asset(__DIR__ . '/../static', true));
?>
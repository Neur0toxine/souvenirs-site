<?php
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../ApiConstruct.php';

class PostApi extends ApiConstruct {
    public function create() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /');
            die();
        }

        $data = $this->request->json;
        if($data) 
            $GLOBALS['posts']->add(['name' => $data->name, 'category' => $data->category, 'content' => $data->content]);
        else {
            $GLOBALS['posts']->add(RequestParser::toArray(['name', 'category', 'content']));
            header('Location: /');
        }
    }

    public function read() {
        $data = $this->request->json;
        if($data) {
            $post = $GLOBALS['posts']->get($data->id);
                $this->request->send([
                    'id' => $post['id'],
                    'name' => $post['name'],
                    'category_id' => $post['category_id'],
                    'category_name' => $post['title'],
                    'content' => $post['contents']
                ]);
        }
    }
    
    public function update() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /');
            die();
        }

        $data = $this->request->json;
        if($data) 
            $GLOBALS['posts']->edit(['id' => $data->id, 'name' => $data->name, 'category' => $data->category, 'content' => $data->content]);
        else {
            $GLOBALS['posts']->edit(RequestParser::toArray(['id', 'name', 'category', 'content']));
            header('Location: /');
        }
    }

    public function delete() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /');
            die();
        }

        $data = $this->request->json;
        if($data) {
            $handler = $GLOBALS['posts']->delete($data->id);
            $this->request->send(['success' => isset($handler), $data->id]);
        }
        else
            $this->request->send(['success' => false]);
    }
}
?>
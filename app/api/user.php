<?php
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../ApiConstruct.php';

class UserApi extends ApiConstruct {
    public function create() {
        $data = $this->request->json;
        $this->request->send(['p'=> $GLOBALS['users']->hashPassword('admin')]);
        if($data) {
            $GLOBALS['users']->add($data->username, $data->password);
        }
    }

    public function read() {
        $data = $this->request->json;
        if($data) {
            $user = $GLOBALS['users']->get($data->id);
            $this->request->send(['username' => $user['username']]);
        }
    }
}
?>
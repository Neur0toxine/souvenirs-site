<?php
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../ApiConstruct.php';

class CategoryApi extends ApiConstruct {
    public function create() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /categories');
            die();
        }

        $data = $this->request->json;
        if($data) 
            $GLOBALS['categories']->add(['title' => $data->title]);
        else {
            $GLOBALS['categories']->add(RequestParser::toArray(['title']));
            header('Location: /categories');
        }
    }

    public function read() {
        $data = $this->request->json;
        if($data) {
            $category = $GLOBALS['categories']->get($data->id);
                $this->request->send([
                    'id' => $category['id'],
                    'title' => $category['title'],
                ]);
        }
    }
    
    public function update() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /');
            die();
        }

        $data = $this->request->json;
        if($data) 
            $GLOBALS['categories']->edit(['id' => $data->id, 'title' => $data->title]);
        else {
            $GLOBALS['categories']->edit(RequestParser::toArray(['id', 'title']));
            header('Location: /categories');
        }
    }

    public function delete() {
        if(!$GLOBALS['users']->isLoggedIn()) {
            header('Location: /');
            die();
        }

        $data = $this->request->json;
        if($data) {
            $handler = $GLOBALS['categories']->delete($data->id);
            $this->request->send(['success' => isset($handler)]);
        }
        else
            $this->request->send(['success' => false]);
    }
}
?>
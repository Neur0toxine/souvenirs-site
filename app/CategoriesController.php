<?php
require_once __DIR__ . '/../vendor/autoload.php';

class CategoriesController {
    function __construct(&$db) {
        if(!isset($db))
            throw new Exception('First parameter must be initialized');
        if(!($db instanceof \LessQL\Database))
            throw new Exception('First parameter must be instance of \LessQL\Database class');
        $this->db = $db;
    }
    
    public function add($category) {
        $row = $this->db->categories()->createRow();
        $row->setData([
            'title' => $category['title']
        ]);
        $row->save();
    }
    
    
    public function edit($category) {
        $row = $this->db->categories()->where('id', $category['id'])->fetch();
        $row->setData([
            'id' => $category['id'],
            'title' => $category['title'],
        ]);
        $row->save();
    }

    public function list() {
        return $this->db->categories()->fetchAll();
    }

    public function get($id) {
        return $this->db->categories()->where('id', $id)->fetch();
    }
    
    public function delete($id) {
        $row = $this->db->categories()->where('id', $id)->fetch();
        $row->delete();
        try {
            $this->db->commit();
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }
}
?>
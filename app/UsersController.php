<?php
require_once __DIR__ . '/../vendor/autoload.php';

class UsersController {

    // **************************************************************************
    // Конструктор принимает объект SafeMySQL
    public function __construct(&$db) {
        if(!isset($db))
            throw new Exception('First parameter must be initialized');
        if(!($db instanceof \LessQL\Database))
            throw new Exception('First parameter must be instance of \LessQL\Database class');
        $this->db = $db;
    }

    // **************************************************************************
    // *                                                                        *
    // *                            Операции CRUD                               *
    // *                                                                        *
    // **************************************************************************
    public function add($username, $password) {
        $row = $this->db->createRow('users', [
            'username' => $username,
            'password' => $this->hashPassword($password)
        ]);

        $this->db->begin();
        $row->save();
        $this->db->commit();
    }

    public function get($id) {
        return $this->db->users()->where('id', $id)->fetch();
    }

    public function getByName($name) {
        return $this->db->users()->where('username', $name)->fetch();
    }

    public function update($id, $username, $password) {
        $this->db->query("UPDATE ?n SET ?n=?s, ?n=?s WHERE ?n=?i", 
            'users', 'username', $username, 'password', $this->hashPassword($password), 'id', $id);
        if($this->db->insertId() == 0)
            return false;
        else
            return true;
    }

    public function delete($id) {
        $this->db->query('DELETE FROM ?n WHERE ?n=?i', 'users', 'id', $id);
        if($this->db->affectedRows() == 0)
            return false;
        else
            return true;
    }
    // **************************************************************************
    // **************************************************************************



    // **************************************************************************
    // *                                                                        *
    // *      Важные функции чтобы не дублировать код (хеширование, др.)        *
    // *                                                                        *
    // **************************************************************************
    public function setPassword(&$user, $password) {
        $user['password'] = $this->hashPassword($password);
    }

    public function checkPassword($user_id, $password) {
        $user = $this->get($user_id);
        if(!isset($user))
            return false;
        return password_verify($password . getenv('SALT', '__SALT__'), $user['password']);
    }

    public function hashPassword($password) {
        return password_hash($password . getenv('SALT', '__SALT__'), PASSWORD_DEFAULT);
    }

    public function generateToken($username, $password_hash) {
        return hash('sha512', $password_hash . $username);
    }
    // **************************************************************************
    // **************************************************************************



    // **************************************************************************
    // *                                                                        *
    // *      Операции для бекенда: проверка входа, получение ника, и др.       *
    // *                                                                        *
    // **************************************************************************
    public function login($username, $password) {
        $user = $this->getByName($username);
        if(!$user)
            throw new Exception('Пользователь не найден');
        if($this->checkPassword($user['id'], $password)) {
            if(session_status() == PHP_SESSION_NONE)
                session_start();
            $_SESSION['user'] = $user['id'];
            $_SESSION['token'] = $this->generateToken($username, $user['password']);
            return 'Авторизация успешна';
        }
        else
            throw new Exception('Неверный пароль');
    }

    public function isLoggedIn() {
        if(session_status() == PHP_SESSION_NONE)
            session_start();
        $user_id = isset($_SESSION['user']) ? $_SESSION['user'] : NULL;
        $user = $this->get($user_id);
        if(!$user)
            return false;
        else {
            if(!isset($_SESSION['token']))
                return false;
            if($_SESSION['token'] == $this->generateToken($user['username'], $user['password']))
                return true;
            else
                return false;
        }
    }

    public function logout() {
        if(session_status() != PHP_SESSION_NONE) {
            session_unset();
            session_destroy();
            return true;
        }
        else
            return false;
    }

    public function getUsername() {
        if(!isset($_SESSION['user']))
            return null;
        else {
            $user = $this->get($_SESSION['user']);
            if(isset($user))
                return $user['username'];
            else
                return null;
        }
    }
    // Конец.
    // **************************************************************************
}
?>
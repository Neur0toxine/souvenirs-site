<?php
require_once __DIR__ . '/../vendor/autoload.php';

class PostsController {
    function __construct(&$db) {
        if(!isset($db))
            throw new Exception('First parameter must be initialized');
        if(!($db instanceof \LessQL\Database))
            throw new Exception('First parameter must be instance of \LessQL\Database class');
        $this->db = $db;
    }

    public function list($page, $perPage, $category) {
        if(isset($category))
            return $this->db->posts()->where('categories_id', $category)->orderBy('id', 'DESC')->paged($perPage, $page)->fetchAll();
        else
            return $this->db->posts()->paged($perPage, $page)->orderBy('id', 'DESC')->fetchAll();        
    }

    public function get($id) {
        $r = $this->db->posts()->where('id', $id);
        $r->categories_id();
        return $r->fetch();
    }

    public function delete($id) {
        $row = $this->db->posts()->where('id', $id)->fetch();
        $row->delete();
        try {
            $this->db->commit();
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }

    public function add($post) {
        $row = $this->db->posts()->createRow();
        $row->setData([
            'name' => $post['name'],
            'categories_id' => $post['category'],
            'contents' => $post['content']
        ]);
        $row->save();
    }
    
    public function edit($post) {
        $row = $this->db->posts()->where('id', $post['id'])->fetch();
        $row->setData([
            'id' => $post['id'],
            'name' => $post['name'],
            'categories_id' => $post['category'],
            'contents' => $post['content']
        ]);
        $row->save();
    }
    // TODO: Сделать работу с постами.
}
?>
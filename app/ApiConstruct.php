<?php
class RequestBody {
    public function __construct($form, $raw) {
        $this->form = $form;
        $this->raw = $raw;
        $this->json = json_decode($raw);
    }

    public function send($arr) {
        echo json_encode($arr);
    }

    public function var($var) {
        if(isset($this->form[$var]))
            return $this->form[$var];
        else
            return null;
    }
}

class ApiConstruct {
    public function __construct($action, $data, $raw) {
        $this->request = new RequestBody($data, $raw);
        $this->$action();
    }
    public function __destruct() {
        unset($this->request);
    }
    public function create() {}
    public function read() {}
    public function update() {}
    public function delete() {}
}
?>
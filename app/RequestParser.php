<?php
class RequestParser {
    public static function toArray($vars, $type = null) {
        $req = null;
        switch($type) {
            case 'get':
                $req = $_GET;
                break;
            case 'post':
                $req = $_POST;
                break;
            default:
                $req = $_REQUEST;
                break;
        }

        $result = array();
        foreach($vars as $v)
            $result[$v] = isset($req[$v]) ? $req[$v] : null;
        return $result;
    }
    
    public static function is($type) {
        return ($_SERVER['REQUEST_METHOD'] == $type);
    }

    public static function get($var) {
        if(isset($_GET[$var]))
            return $_GET[$var];
        else
            return null;
    }

    public static function post($var) {
        if(isset($_POST[$var]))
            return $_POST[$var];
        else
            return null;
    }

    public static function request($var) {
        if(isset($_REQUEST[$var]))
            return $_REQUEST[$var];
        else
            return null;
    }
}
?>
<?
$file = preg_replace('(\W+)', '', $_REQUEST['file']);
$action = preg_replace('(\W+)', '', $_REQUEST['action']);

unset($_REQUEST['file']);
unset($_REQUEST['action']);
$data = array_filter($_REQUEST);
$raw = file_get_contents('php://input');

include __DIR__ . '/api/' . $file . '.php';
$readerName = ucfirst($file) . 'Api';
$reader = new $readerName($action, $data, $raw);
?>
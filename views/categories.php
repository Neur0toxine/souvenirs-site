<?php $this->layout('layout',
    ['title' => 'Категории - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => null]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
    <ol class="list-group">
    <? foreach($categories as $cat): ?>
		  <li class="list-group-item" id="category-<?= $cat->id ?>">
		  	<span><?= $cat->title ?></span>
		  	<div class="right-full">
			  	<div class="btn-group" role="group" aria-label="<?= $cat->title ?>">
				  <a href="/editcategory/<?= $cat->id ?>" type="button" class="btn btn-sm btn-primary d-block">
				  	<i class="fa fa-pencil" aria-hidden="true"></i>
				  	<span>Редактировать</span>
				  </a>
				  <a href="#" onclick="removeCategory(event, <?= $cat->id ?>)" type="button" class="btn btn-sm btn-danger d-block">
				  	<i class="fa fa-times" aria-hidden="true"></i>
				  	<span>Удалить</span>
				  </a>
				</div>
		  	</div>
		  </li>
    <? endforeach; ?>
	</ol>
<? $this->end() ?>
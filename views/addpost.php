<?php $this->layout('layout',
    ['title' => (isset($post->id) ? 'Редактирование' : 'Добавление') . ' поста - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => null]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
<form class="mx-auto my-4" style="max-width:30rem;" method="post" action="/api/post/<?= isset($post->id) ? 'update' : 'create' ?>">
  <? if(strlen($error) > 0): ?>
  <div class="alert alert-danger"><?= $error ?></div>
  <? endif; ?>
  <? if(isset($post->id)): ?>
  <input type="hidden" name="id" value="<?= $post->id ?>">
  <? endif; ?>
  <div class="form-group">
    <label for="name">Заголовок поста</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Введите заголовок" value="<?= isset($post->name) ? $post->name : '' ?>">
  </div>
  <div class="form-group">
    <label for="name">Категория поста</label>
    <select class="form-control" name="category">
    <? foreach($categories as $cat): ?>
    <option value="<?= $cat['id'] ?>"<?= isset($post->categories_id) ? $post->categories_id == $cat['id'] ? ' selected' : '' : '' ?>><?= $cat['title'] ?></option>
    <? endforeach; ?>
    </select>
  </div>
  <div class="form-group">
    <label for="content">Содержание поста</label>
    <div class="btn-group btn-group-sm w-100 mb-1">
        <button class="btn btn-light btn-sm" onclick="e_bold(event)"><i class="fa fa-bold" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm" onclick="e_italic(event)"><i class="fa fa-italic" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm" onclick="e_underline(event)"><i class="fa fa-underline" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm" onclick="e_strike(event)"><i class="fa fa-strikethrough" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm ml-1" onclick="e_header(event)"><i class="fa fa-header" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm" onclick="e_article(event)"><i class="fa fa-paragraph" aria-hidden="true"></i></button>
        <button class="btn btn-light btn-sm" onclick="e_image(event)"><i class="fa fa-image" aria-hidden="true"></i></button>
    </div>
    <textarea name="content" class="form-control" id="content" placeholder="Начните писать здесь..." rows="5"><?= isset($post->contents) ? $post->contents : '' ?></textarea>
  </div>
  <button type="submit" class="btn btn-info">Отправить</button>
</form>
<? $this->end() ?>
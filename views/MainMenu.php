<div class="parallax w-100 mx-0 d-flex flex-row align-items-center justify-content-center">
            <p class="parallax-header"><?= (isset($ptitle) ? $ptitle : 'Сувениры') ?></p>
        </div>
        <div class="main-menu w-100 h-auto mx-0">
            <ul class="nav d-flex flex-row align-items-center justify-content-center w-100 mx-0">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Категории</a>
                    <div class="dropdown-menu">
                        <? if(isset($categories)): ?>
                        <? foreach($categories as $cat): ?>
                        <?
                            $active = '';
                            if(isset($category))
                                if($category == $cat['id'])
                                    $active = ' active';
                        ?>
                        <a class="dropdown-item<?= $active ?>" href="/posts/<?= $cat['id'] ?>"><?= $cat['title'] ?></a>
                        <? endforeach; ?>
                        <? endif; ?>
                    </div>
                </li>
                <? if(!$GLOBALS['users']->isLoggedIn()): ?>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Вход</a>
                </li>
                <? else: ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Управление</a>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="/addpost">Добавить пост</a>
                    <a class="dropdown-item" href="/addcategory">Добавить категорию</a>
                    <a class="dropdown-item" href="/categories">Категории</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/logout">Выход</a>
                    </div>
                </li>
                <? endif; ?>
            </ul>
        </div>
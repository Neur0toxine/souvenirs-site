<? $GLOBALS['bbcode']->parse(trim($post['contents'])); ?>
<div class="d-flex flex-column justify-content-center w-100 mx-2 mb-3" id="post-<?= $post['id'] ?>">
    <a href="/post/<?= $post['id'] ?>"><h1 class="mb-1"><?= $post['name'] ?></h1></a>
    <div class="d-flex flex-row flex-wrap align-items-center mx-2">
        <span class="badge badge-info m-1"><?= $post->categories()->fetch()['title'] ?></span>
        <? if($admin): ?>
            <a class="m-1 ml-3" href="/editpost/<?= $post['id'] ?>">Редактировать</a>
            <a class="m-1" href="#" onclick="removePost(event, <?= $post['id'] ?>)">Удалить</a>
        <? endif; ?>
    </div>
    <div class="p-2 mx-2 postbody<? if(isset($truncate)) echo ' truncated'?>">
        <div class="postcontent<? if(isset($truncate)) echo ' truncated'?>"><?= $GLOBALS['bbcode']->getAsHtml()  ?></div>
        <? if(isset($truncate)): ?>
        <div class="post-truncater">
            <a href="/post/<?= $post['id'] ?>">К полной версии</a>
        </div>
        <? endif; ?>
    </div>
</div>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <? $this->push('meta') ?>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <? $this->end() ?>
        <?= $this->section('meta') ?>

        <title><?= $this->e($title) ?></title>

        <link rel="manifest" href="/site.webmanifest">
        <link rel="apple-touch-icon" href="/<?= $this->asset('icon.png') ?>">

        <? $this->push('css') ?>
        <link rel="stylesheet" href="<?= $this->asset('/css/normalize.css') ?>">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <link rel="stylesheet" href="<?= $this->asset('/css/main.css') ?>">
        <? $this->end() ?>
        <?= $this->section('css') ?>
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">Вы используете <strong>устаревший</strong> браузер. Рекомендуем <a href="https://browsehappy.com/">обновить</a> его.</p>
        <![endif]-->

        <?= $this->section('menu') ?>
        <div class="container main mt-2 mb-4">
            <?= $this->section('contents') ?>
        </div>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">Сувениры 2017</span>
            </div>
        </footer>

        <? $this->push('scripts') ?>
        <script src="<?= $this->asset('/js/libs/modernizr-3.5.0.min.js') ?>"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="<?= $this->asset('/js/libs/jquery-3.2.1.min.js') ?>"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <!-- <script src="<?= $this->asset('/js/plugins.js') ?>"></script> -->
        <script src="https://rawgit.com/timdown/rangyinputs/master/rangyinputs-jquery-src.js"></script>
        <script src="<?= $this->asset('/js/main.js') ?>"></script>
        <? $this->end() ?>
        <?= $this->section('scripts') ?>

        <? $this->push('googletag') ?>
        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <? $this->end() ?>
        <?= $this->section('googletag') ?>
    </body>
</html>

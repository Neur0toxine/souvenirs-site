<?php $this->layout('layout',
    ['title' => (isset($category_name) ?  $category_name : 'Главная') . ' - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => $category]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
<?
foreach($posts as $post) {
    echo $this->fetch('Post', ['post' => $post, 'truncate' => true, 'admin' => $admin]);
}
?>
<? if($admin): ?>
<a href="/addpost<?= isset($category) ? '?category=' . $category : '' ?>" class="d-block btn btn-info mx-auto my-2">Добавить новый пост</a>
<? endif; ?>
<? if(isset($category)): ?>
<a href="/posts/<?= $category ?>/page/<?= $nextpage ?>" class="mt-2" style="display:inline-block">&lt;&lt; К более старым постам</a>
<? else: ?>
<a href="/page/<?= $nextpage ?>" class="mt-2" style="display:inline-block">&lt;&lt; К более старым постам</a>
<? endif ?>
<? $this->end() ?>
<?php $this->layout('layout',
    ['title' => 'Вход - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => null]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
<form class="mx-auto my-4" style="max-width:30rem;" method="post" action="/login">
  <? if(strlen($error) > 0): ?>
  <div class="alert alert-danger"><?= $error ?></div>
  <? endif; ?>
  <div class="form-group">
    <label for="username">Логин</label>
    <input type="text" name="username" class="form-control" id="username" placeholder="Введите логин">
  </div>
  <div class="form-group">
    <label for="password">Пароль</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="Введите пароль">
  </div>
  <button type="submit" class="btn btn-info">Войти</button>
</form>
<? $this->end() ?>
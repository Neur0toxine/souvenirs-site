<?php $this->layout('layout', ['title' => $post->name . ' - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => $category]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
<?= $this->fetch('Post', ['post' => $post, 'admin' => $admin]) ?>
<? $this->end() ?>
<?php $this->layout('layout',
    ['title' =>  (isset($category->id) ? 'Редактирование' : 'Добавление') . ' категории - ' . (isset($ptitle) ? $ptitle : 'Сувениры')]) ?>
<? $this->start('menu') ?>
<?= $this->fetch('MainMenu', ['categories' => $categories, 'category' => null]) ?>
<? $this->end() ?>
<? $this->start('contents') ?>
<form class="mx-auto my-4" style="max-width:30rem;" method="post" action="/api/category/<?= isset($category->id) ? 'update' : 'create' ?>">
  <? if(strlen($error) > 0): ?>
  <div class="alert alert-danger"><?= $error ?></div>
  <? endif; ?>
  <? if(isset($category->id)): ?>
  <input type="hidden" name="id" value="<?= $category->id ?>">
  <? endif; ?>
  <div class="form-group">
    <label for="name">Название категории</label>
    <input type="text" name="title" class="form-control" id="name" placeholder="Введите название" value="<?= isset($category->title) ? $category->title : '' ?>">
  </div>
  <button type="submit" class="btn btn-info">Отправить</button>
</form>
<? $this->end() ?>
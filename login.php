<?php
require_once __DIR__ . '/app/config.php';
if($GLOBALS['users']->isLoggedIn()) {
    header('Location: /');
    die();
}

$error = '';
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $GLOBALS['users']->login(RequestParser::post('username'), RequestParser::post('password'));
        header('Location: /');
    }
    catch(Exception $e) {
        $error = $e->getMessage();
    }
}

echo $tpl->render('login', 
    [
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'categories' => $GLOBALS['categories']->list(),
        'error' => $error
    ]
);
?>
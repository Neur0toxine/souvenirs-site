<?php
require_once __DIR__ . '/app/config.php';
if(!$GLOBALS['users']->isLoggedIn()) {
    header('Location: /');
    die();
}
echo $tpl->render('addcategory', 
    [
        'error' => '',
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'category' => $GLOBALS['categories']->get(RequestParser::get('id')),
        'categories' => $GLOBALS['categories']->list(),
        'admin' => $GLOBALS['users']->isLoggedIn(),
    ]
);
?>
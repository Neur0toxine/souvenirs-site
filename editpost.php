<?php
require_once __DIR__ . '/app/config.php';
if(!$GLOBALS['users']->isLoggedIn()) {
    header('Location: /');
    die();
}
echo $tpl->render('addpost', 
    [
        'error' => '',
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'post' => $GLOBALS['posts']->get(RequestParser::get('id')),
        'category' => RequestParser::get('category'),
        'categories' => $GLOBALS['categories']->list(),
        'admin' => $GLOBALS['users']->isLoggedIn(),
    ]
);
?>
<?php
require_once __DIR__ . '/app/config.php';
$category = isset($_GET['category']) ? $_GET['category'] : null;
echo $tpl->render('postpage', 
    [
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'categories' => $GLOBALS['categories']->list(),
        'category' => $category,
        'post' => $GLOBALS['posts']->get($_GET['id']),
        'admin' => $GLOBALS['users']->isLoggedIn()
    ]
);
?>
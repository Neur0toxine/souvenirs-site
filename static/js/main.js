$(window).bind('scroll', function () {
    if ($(window).scrollTop() > ($('.parallax').outerHeight() + $('.main-menu').outerHeight())) {
        if($('.main-menu').is(':animated'))
            return false;
        $('.main-menu').css('top', '-' + $('.main-menu').outerHeight() + 'px');
        $('.main-menu').addClass('fixed');
        $('.main-menu').animate({top:'0px'}, { duration: 180, specialEasing: {height: "easeOutBounce"}});
    } else {
        $('.main-menu').css('top', '-' + $('.main-menu').outerHeight() + 'px');
        $('.main-menu').removeClass('fixed');
    }
});

$('body').on('mouseenter mouseleave','.dropdown',function(e){
    var _d=$(e.target).closest('.dropdown');_d.addClass('show');
    setTimeout(function(){
      _d[_d.is(':hover')?'addClass':'removeClass']('show');
    },300);
  });

$(document).ready(function(){
    $(window).trigger('scroll');
})

function removePost(e, post_id) {
    e.preventDefault();
    if(confirm('Вы действительно хотите удалить этот пост?')) {
        $.post( "/api/post/delete", JSON.stringify({ id: post_id }), function( data ) {
            var d = JSON.parse(data);
            if(d.success)
                $('#post-' + post_id).remove();
            else
                alert('Ошибка удаления поста')
        });
    }
}

function removeCategory(e, cat_id) {
    e.preventDefault();
    if(confirm('Вы действительно хотите удалить эту категорию?')) {
        $.post( "/api/category/delete", JSON.stringify({ id: cat_id }), function( data ) {
            var d = JSON.parse(data);
            if(d.success)
                $('#category-' + cat_id).remove();
            else
                alert('Ошибка удаления категории')
        });
    }
}

function e_bold(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[b]' + extracted + '[/b]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_italic(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[i]' + extracted + '[/i]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_underline(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[u]' + extracted + '[/u]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_strike(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[s]' + extracted + '[/s]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_header(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[header]' + extracted + '[/header]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_article(e) {
    e.preventDefault();
    var extracted = $("#content").extractSelectedText();
    extracted = '[p]' + extracted + '[/p]';
    $("#content").replaceSelectedText(extracted, "select");
}

function e_image(e) {
    e.preventDefault();
    var uri = prompt('Введите ссылку на картинку');
    if(!uri) return false;
    $("#content").replaceSelectedText('[image]' + uri + '[/image]', "select");
}
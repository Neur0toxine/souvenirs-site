<?php
require_once __DIR__ . '/app/config.php';
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$category = isset($_GET['category']) ? $_GET['category'] : null;
echo $tpl->render('index', 
    [
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'posts' => $GLOBALS['posts']->list($page, 10, $category),
        'categories' => $GLOBALS['categories']->list(),
        'category' => $category,
        'category_name' => $GLOBALS['categories']->get($category)['title'],
        'admin' => $GLOBALS['users']->isLoggedIn(),
        'nextpage' => $page + 1
    ]
);
?>
<?php
require_once __DIR__ . '/app/config.php';
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$category = isset($_GET['category']) ? $_GET['category'] : null;
echo $tpl->render('categories', 
    [
        'ptitle' => getenv('TITLE', 'Сувениры'),
        'categories' => $GLOBALS['categories']->list(),
        'admin' => $GLOBALS['users']->isLoggedIn(),
    ]
);
?>